function StatusBar(data) {
	var dynamicValueContainers;

	if(!data) data = defaultData();
	includeModuleSpecificCss();
	insertMainStatusBarContainer();
	initStatusBarParts(data);
	dynamicValueContainers = getDynamicValueContainers();
	updateOnEvents(['apaChanged']);
//	setTimeout(updateDynamicValues(apa),1); // monkeyPatch for google chrome load sequence

	function defaultData(){
		return {left:"Please configure this plugin like described in plugin/status-bar/README.md"};
	}
	function includeModuleSpecificCss(){
		var statusBarCss = document.createElement("link");
		statusBarCss.setAttribute("rel","stylesheet");
		statusBarCss.setAttribute("href","plugin/status-bar/status-bar.css");
		document.head.appendChild(statusBarCss);
	}
	function insertMainStatusBarContainer(){
		var statusBarElement=document.createElement("div");
		statusBarElement.className = 'statusBar';
		document.body.appendChild(statusBarElement);
	}
	function initStatusBarParts(data){
		for (var statusBarPartName in data) {
			var statusBarPartContent = data[statusBarPartName];
			convertedContent = convertKeywordsToDynamicValues(statusBarPartContent);
			insertStatusBarPart(statusBarPartName, convertedContent);
		}
	}
	function convertKeywordsToDynamicValues(keywordedString){
		var keywordDetector = new RegExp('{{(.[|])?([a-zA-Z0-9._]+)}}','g');
		var readyForUpdateHtmlString = keywordedString.replace(keywordDetector,keywordConvertor);
		return readyForUpdateHtmlString;
	}
	function keywordConvertor(stringMatch, optionalPrefix,keywordName){
		var optionalTag = '';
		if(optionalPrefix) optionalTag =' prefix="'+optionalPrefix.substring(0,optionalPrefix.length-1)+'"';
		return '<span class="'+keywordName+'"'+optionalTag+'></span>';
	}
	function insertStatusBarPart(className, htmlContent){
		var part = document.createElement("div");
		part.className = className;
		part.innerHTML = htmlContent;
		document.querySelector('.statusBar').appendChild(part);
	}
	function getDynamicValueContainers(){
		return document.querySelectorAll('.statusBar>div span');
	}
	function updateOnEvents(eventTabList){
		var revealEventTransmitter = document.querySelector( '.reveal' );
		for (var i in eventTabList){
			revealEventTransmitter.addEventListener(eventTabList[i], updateDynamicValues);
		}
	}
	function updateDynamicValues(eventData){
		containerList = dynamicValueContainers;
		for (var index=0; index<containerList.length;index++){
			var dataType = containerList[index].className;
			updateDynamicValue(containerList[index], eventData[dataType]);
		}
	}
	function updateDynamicValue(valueContainer,value){
		var optionalPrefix = valueContainer.getAttribute('prefix');
		if(optionalPrefix){
			valueContainer.innerHTML = optionalPrefix + value;
			displayIfValue(valueContainer, value);
		} else {
			valueContainer.innerHTML = value;
		}
	}
	function displayIfValue(valueContainer, value){
		if(value){
			valueContainer.style.display = 'inline';
		} else {
			valueContainer.style.display = 'none';
		}
	}
}

