#Test
describe 'Custom Metric Handler', ->
	it 'Il peut fusionner deux objets.', ->
		base =
			toto : 'toto'
		bonux =
			tata : 'tata'
		extend(base, bonux)

		base.should.eql
			toto : 'toto'
			tata : 'tata'
